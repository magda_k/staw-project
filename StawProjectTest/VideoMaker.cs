﻿using Microsoft.Azure.WebJobs.Host;
using NReco.VideoConverter;
using System;
using System.IO;

namespace StawProjectTest
{
    public class VideoMaker
    {
        private readonly VideoFileSettings _settings;
        private readonly FFMpegConverter _videoConverter;

        public VideoMaker(VideoFileSettings settings)
        {
            _settings = settings;
            _videoConverter = new FFMpegConverter();
            _videoConverter.FFMpegToolPath = Path.Combine(_settings.ApplicationPath, "ffmpeg", "bin");
        }

        public VideoFile CreateVideo(TraceWriter log)
        {
            ConvertSettings convertSettings = new ConvertSettings()
            { 
                CustomInputArgs = "-f image2 "
			};

			var inputPath = Path.Combine(
                _settings.InputDirectoryPath,
                _settings.FileNamePattern);

            if (!Directory.Exists(_settings.OutputDirectoryPath))
            {
                Directory.CreateDirectory(_settings.OutputDirectoryPath);
            }

            string filename = $"stawy_{DateTime.Now.ToString("yyyyMMddHHmmss")}.mp4";
            string outputFilePath = Path.Combine(_settings.OutputDirectoryPath, filename);
            try
            {
                log.Info("Creating video..");
                _videoConverter.ConvertMedia(inputPath, null, outputFilePath, null, convertSettings);
                log.Info($"Video created at: {outputFilePath}");
            }
            catch (FFMpegException ex)
            {
                log.Error("There was an error while creating video", ex);
                throw;
            }

            return new VideoFile()
            {
                FileName = filename,
                FilePath = outputFilePath
            };
        }
    }
}
