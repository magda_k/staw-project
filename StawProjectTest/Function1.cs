using System;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;

namespace StawProjectTest
{
    public static class Function1
    {
        [FunctionName("Function1")]
        public static void Run([TimerTrigger("0 0 */1 * * *")]TimerInfo myTimer, TraceWriter log, ExecutionContext context)
        {
            RunAsync(myTimer, log, context).GetAwaiter().GetResult();
        }

        public static async Task RunAsync(TimerInfo myTimer, TraceWriter log, ExecutionContext context)
        {
            var storageInputConnectionString = Environment.GetEnvironmentVariable("StorageInputConnectionString");

            BlobSettings blobSettings = new BlobSettings()
            {
                StorageConnectionString = storageInputConnectionString,
                StorageContainerName = "obrazki",
            };

            VideoFileSettings outputSettings = new VideoFileSettings()
            {
                ApplicationPath = context.FunctionAppDirectory,
                FileNamePattern = "stawy_%06d.jpg",
				FileNumberPlaceholder = "%06d",
                OutputDirectory = "Output",
                InputDirectory = "Input",
            };

			BlobDownloader downloader = new BlobDownloader(blobSettings, outputSettings);
            await downloader.DownloadImages(log);

            VideoMaker videoMaker = new VideoMaker(outputSettings);
            VideoFile videofile= videoMaker.CreateVideo(log);

            var storageOutputConnectionString = Environment.GetEnvironmentVariable("StorageOutputConnectionString");

            BlobSettings settings = new BlobSettings()
            {
                StorageConnectionString = storageOutputConnectionString,
                StorageContainerName = "filmy",
            };

            BlobUploader uploader = new BlobUploader(settings);
            await uploader.UploadFile(videofile, log);
        }

    }
}
