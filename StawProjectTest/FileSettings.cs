﻿using System.IO;

namespace StawProjectTest
{
    public class VideoFileSettings
    {
        public string FileNamePattern { get; internal set; }
        public string ApplicationPath { get; internal set; }
        public string OutputDirectory { get; internal set; }
        public string InputDirectory { get; internal set; }

        public string InputDirectoryPath => Path.Combine(ApplicationPath, InputDirectory);
        public string OutputDirectoryPath => Path.Combine(ApplicationPath, OutputDirectory);
	    public string FileNumberPlaceholder { get; set; }
    }
}
