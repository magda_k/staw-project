﻿namespace StawProjectTest
{
    public class VideoFile
    {
        public string FileName { get; set; }
        public string FilePath { get; set; }
    }
}
