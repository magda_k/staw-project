﻿using Microsoft.Azure.WebJobs.Host;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Threading.Tasks;

namespace StawProjectTest
{
    public class BlobUploader
    {
        private readonly BlobSettings _settings;
        private readonly CloudBlobClient _client;

        private const string _fileName = "stawy_latest.mp4";

        public BlobUploader(BlobSettings settings)
        {
            var storageAccount = CloudStorageAccount.Parse(settings.StorageConnectionString);
            _settings = settings;
            _client = storageAccount.CreateCloudBlobClient();
        }

        public async Task UploadFile(VideoFile videoFile, TraceWriter log)
        {
            CloudBlobContainer cloudBlobContainer = _client.GetContainerReference(_settings.StorageContainerName);
            await cloudBlobContainer.CreateIfNotExistsAsync();

            string fileName = _fileName;
            log.Info($"Uploading video {fileName} to storage..");
            CloudBlockBlob cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(fileName);
            await cloudBlockBlob.UploadFromFileAsync(videoFile.FilePath);
            log.Info("Video uploaded.");
        }
    }
}
