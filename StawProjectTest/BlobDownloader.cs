﻿using Microsoft.Azure.WebJobs.Host;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.IO;
using System.Threading.Tasks;

namespace StawProjectTest
{
    public class BlobDownloader
    {
        private readonly BlobSettings _blobSettings;
        private readonly VideoFileSettings _outputSettings;
        private readonly CloudBlobClient _client;

        public BlobDownloader(BlobSettings blobSettings, VideoFileSettings outputSettings)
        {
            var storageAccount = CloudStorageAccount.Parse(blobSettings.StorageConnectionString);
            _client = storageAccount.CreateCloudBlobClient();
            _blobSettings = blobSettings;
            _outputSettings = outputSettings;
        }

        public async Task DownloadImages(TraceWriter log)
        {
            CloudBlobContainer cloudBlobContainer = _client.GetContainerReference(_blobSettings.StorageContainerName);
            BlobContinuationToken blobContinuationToken = null;
            do
            {
                var results = await cloudBlobContainer.ListBlobsSegmentedAsync(null, blobContinuationToken);

                blobContinuationToken = results.ContinuationToken;
                int i = 0;
                foreach (IListBlobItem item in results.Results)
                {
                    CloudBlockBlob cloudBlockBlob = (CloudBlockBlob)item;

	                if (cloudBlockBlob.Properties.Length == 0)
	                {
						continue;
	                }

                    string fileName = _outputSettings.FileNamePattern.Replace(_outputSettings.FileNumberPlaceholder, i.ToString("D6"));
    
                    if (!Directory.Exists(_outputSettings.InputDirectoryPath))
                    {
                        Directory.CreateDirectory(_outputSettings.InputDirectoryPath);
                    }

                    string filePath = Path.Combine(
                        _outputSettings.InputDirectoryPath, 
                        fileName);

                    log.Info($"Downloading blob to {filePath}");
                    await cloudBlockBlob.DownloadToFileAsync(filePath, FileMode.Create);
                    i++;
                }
                blobContinuationToken = results.ContinuationToken;
            } while (blobContinuationToken != null);
        }
    }
}
