﻿namespace StawProjectTest
{
    public class BlobSettings
    {
        public string StorageConnectionString { get; internal set; }
        public string StorageContainerName { get; internal set; }
    }
}
